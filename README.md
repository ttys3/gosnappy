1. ## Prerequisite
   snappy lib should be installed first

2. ## Install
```bash
cd gosnappy
make && make install
```

3. ## usage:
```bash
go get -v github.com/ihacklog/gosnappy
```
 
4. ## API Description

    a) IO style--the same as standard package "compress"

```go
//import "github.com/ihacklog/gosnappy"
gosnappy.NewReader(r io.Reader)(r io.Reader)
gosnappy.NewWriter(w io.Writer)(w io.WriteCloser)
````

   user must call w.Close() at the end of write.
   w.Close does not close the underlined writer
   
   b) Byte array  style-- the same as original snappy lib

```go
gosnappy.Compress(output []byte,input []byte) (nRet int, errRet error)
gosnappy.Decompress(output []byte,input []byte) (nRet int, errRet error)
````

   we could pass nil or small byte array to 'output' parameter of these functions,
   if the capacity of output less than the result size , function will
   automaticly malloc new byte array which is large enough.

   therefor the 'result' object returned may be not the same as 'output'
   object
    
