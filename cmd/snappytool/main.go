//copy from https://github.com/mattn/snappy/raw/35a8406c2110ed6d335c26bec02c0efe4d24b53b/cmd/snappytool/main.go
package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	snappy "github.com/ihacklog/gosnappy"
)

var (
	enc = flag.Bool("e", false, "encode")
	dec = flag.Bool("d", false, "decode")
)

func run() int {
	flag.Parse()
	if *enc == *dec {
		fmt.Fprintf(os.Stderr, "exactly one of -d or -e must be given")
		return 1
	}

	// Encode or decode stdin, and write to stdout.
	var err error
	writer := snappy.NewWriter(os.Stdout)
	if *enc {
		_, err = io.Copy(writer, os.Stdin)
	} else {
		_, err = io.Copy(os.Stdout, snappy.NewReader(os.Stdin))
	}
	if err != nil {
		fmt.Fprintf(os.Stderr, "err:%s\n", err.Error())
		return 1
	}
	return 0
}

func main() {
	os.Exit(run())
}
