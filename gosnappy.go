/*
 * =====================================================================================
 *
 *       Filename:  gosnappy.go
 *
 *    Description:  snappy-c golang bindings
 * ( snappy-c is different with snappy cpp version, it is without frame compress support.)
 *
 *			some implements ref to https://github.com/dgryski/go-csnappy/blob/master/csnappy.go
 *
 *        Version:  1.0.0
 *        Created:  2018-04-07
 *
 *         Author:  HuangYeWuDeng
 *
 * =====================================================================================
 */
package gosnappy

/*
#cgo LDFLAGS: -lsnappy

#include <snappy-c.h>
#include <stdlib.h>
*/
import "C"
import (
	"errors"
	"fmt"
	"io"
	"strconv"
	"unsafe"
)

/*
typedef enum {
  SNAPPY_OK = 0,
  SNAPPY_INVALID_INPUT = 1,
  SNAPPY_BUFFER_TOO_SMALL = 2
} snappy_status;
*/

// Errno is a snappy error
type Errno int

var errText = map[Errno]string{
	0: "ok",
	1: "invalid input",
	2: "buffer too small",
}

func (e Errno) Error() string {
	s := errText[e]
	if s == "" {
		return "snappy errno " + strconv.Itoa(int(e))
	}
	return s
}

// Errors returned from the package
var (
	ErrOk             = Errno(0)
	ErrInvalidInput   = Errno(1)
	ErrBufferTooSmall = Errno(2)
)

//const obufLen = 76490
const obufLen = 65536

const maxBlockSize = 65536

var ErrCorrupt = errors.New("snappy: corrupt input")

var errClosed = errors.New("snappy: Writer is closed")

// NewReader returns a new Reader that decompresses from r, using the framing
// format described at
// https://github.com/google/snappy/blob/master/framing_format.txt
func NewReader(r io.Reader) *Reader {
	return &Reader{
		r:       r,
		decoded: make([]byte, maxBlockSize),
		buf:     make([]byte, obufLen),
	}
}

// Reader is an io.Reader that can read Snappy-compressed bytes.
type Reader struct {
	r       io.Reader
	err     error
	decoded []byte
	buf     []byte
	// decoded[i:j] contains decoded bytes that have not yet been passed on.
	i, j int
}

// NewWriter returns a new Writer that compresses to w.
//
// The Writer returned does not buffer writes. There is no need to Flush or
// Close such a Writer.
func NewWriter(w io.Writer) *Writer {
	return &Writer{
		w:    w,
		obuf: make([]byte, obufLen),
	}
}

// Writer is an io.Writer that can write Snappy-compressed bytes.
type Writer struct {
	w   io.Writer
	err error

	// ibuf is a buffer for the incoming (uncompressed) bytes.
	//
	// Its use is optional. For backwards compatibility, Writers created by the
	// NewWriter function have ibuf == nil, do not buffer incoming bytes, and
	// therefore do not need to be Flush'ed or Close'd.
	ibuf []byte

	// obuf is a buffer for the outgoing (compressed) bytes.
	obuf []byte
}

// 压缩 (输入: 上层write输入的p, 输出：底层Write的p 参数)
func (w *Writer) Write(p []byte) (n int, err error) {
	origLen := len(p)
	compressed, n, err := Compress(w.obuf, p)
	if err != nil {
		n = 0
		w.err = errors.New("snappy Write: " + err.Error())
		return
	}
	if _, err := w.w.Write(compressed[:n]); err != nil {
		w.err = errors.New("snappy Write: " + err.Error())
	}
	//prevent ErrShortWrite
	//https://golang.org/pkg/io/#pkg-variables
	//ErrShortWrite means that a write accepted fewer bytes than requested but failed to return an explicit error.
	n = origLen
	return
}

// Read satisfies the io.Reader interface.
// 解压缩, 输入：底层Read出来的p， 输出：copy给当前p
func (r *Reader) Read(p []byte) (nRet int, err error) {
	totalLen, err := r.r.Read(r.buf)
	if err != nil {
		if err != io.EOF {
			return
		}
	}
	if totalLen == 0 && err == io.EOF {
		nRet = 0
		return
	}
	//slice bounds out of range
	decompressed, nRet, err := Decompress(r.decoded, r.buf[:totalLen])
	if err != nil {
		nRet = 0
		r.err = errors.New("snappy Read: " + err.Error())
		return
	}
	copy(p, decompressed[:nRet])
	return
}

func c_pchar(p *byte) *C.char {
	return (*C.char)(unsafe.Pointer(p))
}

func Compress(output []byte, input []byte) (ret []byte, nRet int, errRet error) {
	size := C.snappy_max_compressed_length(C.size_t(len(input)))
	if C.size_t(cap(output)) < size {
		output = make([]byte, size)
	}
	if C.snappy_compress(c_pchar(&input[0]),
		C.size_t(len(input)),
		c_pchar(&output[0]), &size) != C.SNAPPY_OK {
		errRet = errors.New("snappy Compress: Unexpected Error")
	}
	nRet = int(size)
	ret = output[:nRet]
	return
}

func Decompress(output []byte, input []byte) (ret []byte, nRet int, errRet error) {
	var size C.size_t
	var cRet C.snappy_status
	cRet = C.snappy_uncompressed_length(c_pchar(&input[0]),
		C.size_t(len(input)), &size)

	if cRet != C.SNAPPY_OK {
		errRet =  errors.New("snappy Decompress: Invalid Input")
		return
	}
	if C.size_t(cap(output)) < size {
		output = make([]byte, size)
	}
	//log.Printf("input:%d, output: %d, size: %d\n", len(input), len(output), size)
	//log.Printf("input:%v, output: %v\n", &input[0], &output[0])
	cRet = C.snappy_uncompress(c_pchar(&input[0]),
		C.size_t(len(input)),
		c_pchar(&output[0]), &size)

	if cRet != C.SNAPPY_OK && size <= 0 {
		errRet = errors.New(fmt.Sprintf("snappy Decompress: snappy_uncompress failed. err: %s", Errno(cRet)))
		return
	}
	nRet = int(size)
	ret = output[:nRet]
	errRet = nil
	return
}

// MaxEncodedLen returns the maximum length of a snappy block, given its
// uncompressed length.
func MaxEncodedLenC(srcLen int) int {
	return int(C.snappy_max_compressed_length(C.size_t(srcLen)))
}

// copy from github.com/golang/snappy/encode.go
// MaxEncodedLen returns the maximum length of a snappy block, given its
// uncompressed length.
//
// It will return a negative value if srcLen is too large to encode.
func MaxEncodedLen(srcLen int) int {
	n := uint64(srcLen)
	if n > 0xffffffff {
		return -1
	}
	// Compressed data can be defined as:
	//    compressed := item* literal*
	//    item       := literal* copy
	//
	// The trailing literal sequence has a space blowup of at most 62/60
	// since a literal of length 60 needs one tag byte + one extra byte
	// for length information.
	//
	// Item blowup is trickier to measure. Suppose the "copy" op copies
	// 4 bytes of data. Because of a special check in the encoding code,
	// we produce a 4-byte copy only if the offset is < 65536. Therefore
	// the copy op takes 3 bytes to encode, and this type of item leads
	// to at most the 62/60 blowup for representing literals.
	//
	// Suppose the "copy" op copies 5 bytes of data. If the offset is big
	// enough, it will take 5 bytes to encode the copy op. Therefore the
	// worst case here is a one-byte literal followed by a five-byte copy.
	// That is, 6 bytes of input turn into 7 bytes of "compressed" data.
	//
	// This last factor dominates the blowup, so the final estimate is:
	n = 32 + n + n/6
	if n > 0xffffffff {
		return -1
	}
	return int(n)
}

// DecodedLen returns the length of the decoded block.
func DecodedLen(src []byte) (int, error) {

	result := 0

	err := C.snappy_uncompressed_length((*C.char)(unsafe.Pointer(&src[0])), C.size_t(len(src)), (*C.size_t)(unsafe.Pointer(&result)))

	if err != C.SNAPPY_OK {
		return 0, Errno(err)
	}

	return result, nil
}
