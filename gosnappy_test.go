package gosnappy_test

import "testing"
import "bytes"
import "io/ioutil"
import (
	snappy "github.com/ihacklog/gosnappy"
	"io"
)

var s = "==hello world,hello world,hello world=="

func TestSnappyIO(t *testing.T) {
	var wbytes bytes.Buffer
	w := snappy.NewWriter(&wbytes)

	if _, err := io.Copy(w, bytes.NewBufferString(s)); err != nil {
		t.Error(err)
	}

	r := snappy.NewReader(&wbytes)
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		t.Error(err)
	}

	t.Logf("\ns:<%s>\nbs:<%s>", s, bs)
	if string(bs) != s {
		t.Error("TestSnappyIO: compress-decompress cycle not matched")
	}

}

func TestSnappBytes(t *testing.T) {

	maxCompLen := snappy.MaxEncodedLen(len(s))
	maxCompLenC := snappy.MaxEncodedLenC(len(s))
	if maxCompLen != maxCompLenC {
		t.Error(maxCompLen, maxCompLenC)
	}
	var bytes = make([]byte, maxCompLen)
	_, n, err := snappy.Compress(bytes, []byte(s))
	if n > maxCompLen {
		t.Error(err)
	}

	decodedLen, _ := snappy.DecodedLen(bytes)
	var bytesPlain = make([]byte, decodedLen)
	_,n, err = snappy.Decompress(bytesPlain, bytes)
	if err != nil {
		t.Error(err)
	}
	if string(bytesPlain) != s {
		t.Error("TestSnappBytes: compress-decompress cycle not matched")
	}
}


func TestSnappBigFile(t *testing.T) {

	file := "sample.txt"
	//f, err := os.Open("/tmp/dat")

	dat, _ := ioutil.ReadFile(file)

	maxCompLen := snappy.MaxEncodedLen(len(dat))
	maxCompLenC := snappy.MaxEncodedLenC(len(dat))
	if maxCompLen != maxCompLenC {
		t.Error(maxCompLen, maxCompLenC)
	}
	var bytes = make([]byte, maxCompLen)
	_,n, err := snappy.Compress(bytes, []byte(dat))
	if n > maxCompLen {
		t.Error(err)
	}

	decodedLen, _ := snappy.DecodedLen(bytes)
	var bytesPlain = make([]byte, decodedLen)
	_,n, err = snappy.Decompress(bytesPlain, bytes)
	if err != nil {
		t.Error(err)
	}

	if n != len(dat) {
		t.Error("compress-decompress cycle not matched")
	}
}

func TestSnappyIOBigFileCompress(t *testing.T) {

	file := "sample.txt"
	//f, err := os.Open("/tmp/dat")

	dat, _ := ioutil.ReadFile(file)

	var wbytes bytes.Buffer
	w := snappy.NewWriter(&wbytes)

	//test compress
	if _, err := io.Copy(w, bytes.NewBuffer(dat)); err != nil {
		t.Error(err)
	}

}

func TestSnappyIOBigFileUncompress(t *testing.T) {

	file := "sample.txt"
	//f, err := os.Open("/tmp/dat")

	dat, _ := ioutil.ReadFile(file)

	var wbytes bytes.Buffer
	w := snappy.NewWriter(&wbytes)

	//test compress
	if _, err := io.Copy(w, bytes.NewBuffer(dat)); err != nil {
		t.Error(err)
	}

	var emptybytes bytes.Buffer
	emptyw := snappy.NewWriter(&emptybytes)
	if _, err := io.Copy(emptyw, snappy.NewReader(&wbytes)); err != nil {
		t.Error(err)
	}

}