MK_DIR=./cmd/snappytool

all:
	cd ${MK_DIR}; \
	./build.sh; \
	chmod a+x snappytool snappytool-go; \
	ls --color -lhp ./

install:
	cd ${MK_DIR}; \
	cp -av snappytool ${GOPATH}/bin/; \
	cp -av snappytool-go ${GOPATH}/bin/

clean:
	cd ${MK_DIR}; \
	rm -rf snappytool; \
	rm -rf snappytool-go