#!/bin/sh

dd if=/dev/urandom of=sample.txt bs=10K count=1
go test

dd if=/dev/urandom of=sample.txt bs=30K count=1
go test

dd if=/dev/urandom of=sample.txt bs=50K count=1
go test
